#include "color_to_greyscale.h"
#include "pbm.h"

/* Source: 
   http://www.johndcook.com/blog/2009/08/24/algorithms-convert-color-grayscale/ 
*/

/* G = (max(R, G, B) + min(R, G, B)) / 2 */
void colorToGrayscaleLightness(Image * im)
{
    int i, j;

    Pixel tmp;

    for (i = 0; i < im->height; i++)
	for (j = 0; j < im->width; j++)
	{
	    tmp = im->pixels[i][j];
	    
	    im->pixels[i][j] = 
		createPixelGreyscale((MAX(tmp.r, MAX(tmp.g, tmp.b)) 
				   + MIN(tmp.r, MIN(tmp.g, tmp.b))) >> 1);  
	}
}


/* G = (R + G + B) / 3 */
void colorToGrayscaleAverage(Image * im)
{
    int i, j;

    Pixel tmp;

    for (i = 0; i < im->height; i++)
	for (j = 0; j < im->width; j++)
	{
	    tmp = im->pixels[i][j];
	    
	    im->pixels[i][j] = createPixelGreyscale((tmp.r + tmp.g + tmp.b) / 3);  
	}
}


/* G = 0.21 * R + 0.71 * G + 0.07 * B */
void colorToGrayscaleLuminosity(Image * im)
{
    int i, j;

    Pixel tmp;

    for (i = 0; i < im->height; i++)
	for (j = 0; j < im->width; j++)
	{
	    tmp = im->pixels[i][j];
	    
	    im->pixels[i][j] = 
		createPixelGreyscale((21 * tmp.r + 71 * tmp.g + 8 * tmp.b) 
				/ 100);  
	}
}
