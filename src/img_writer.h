#ifndef HEADER_IMG_WRITER
#define HEADER_IMG_WRITER

#include <stdio.h>
#include "pbm.h"

void writeImage(const char * path, Image * im);

#endif
