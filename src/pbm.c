#include "pbm.h"


Pixel createPixel(component red, component green, component blue)
{
    Pixel p;
    p.r = red;
    p.g = green;
    p.b = blue;
    
    return p;
}

Pixel createPixelGreyscale(component c)
{
    return createPixel(c, c, c);
}

Image * createImage(Format format, int width, int height)
{
    Image * im = malloc(sizeof(Image));
    int i, j;
    
    if (!im) {
        printf("Cannot create image of size %ix%i.\n", width, height);
        return NULL;
    }
    
    im->origin_format = format;
    im->width = width;
    im->height = height;
    
    im->pixels = malloc(height * sizeof(Pixel*));
    
    if (!im->pixels) {
        printf("Cannot create lines in image of size %ix%i.\n",
               width, height);
        removeImage(im);
        return NULL;
    }
    
    for (i = 0; i < height; ++i) {
        im->pixels[i] = malloc(width * sizeof(Pixel));
        
        if (!im->pixels[i]) {
            printf("Cannot create pixel in image of size %ix%i.\n",
                   width, height);
            removeImage(im);
            return NULL;
        }
        
        for (j = 0; j < width; ++j)
            im->pixels[i][j] = createPixel(0, 0, 0);
    }
    
    return im;
}

void removeImage(Image * im)
{
    int i;
    
    if (im) {
        if (im->pixels) {
            for (i = 0; i < im->height; ++i)
                if (im->pixels[i])
                    free(im->pixels[i]);
            
            free(im->pixels);
        }
        
        free(im);
    }
}

Image * copyImage(Image * im)
{
  Image * copy = createImage(im->origin_format, im->width, im->height);
  int i, j;
  
  for (i = 0; i < copy->height; ++i)
    for (j = 0; j < copy->width; ++j) {
      copy->pixels[i][j].r = im->pixels[i][j].r;
      copy->pixels[i][j].g = im->pixels[i][j].g;
      copy->pixels[i][j].b = im->pixels[i][j].b;
    }
  
  copy->max_value = im->max_value;
  
  return copy;
}

bool isPixelValid(int l, int c, Image * im)
{
  return ((l >= 0) && (l < im->height) && (c >= 0) && (c < im->width));
}
