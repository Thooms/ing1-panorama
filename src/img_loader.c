#include "img_loader.h"

char * _readNextLine(FILE * fp)
{
    char * str = malloc(256 * sizeof(char));
    
    if (!str) {
        printf("Error creating string...\n");
        return NULL;
    }
    
    do {
        //fscanf(fp, "\n%[^\n]", str);
        fgets(str, 70, fp); // 70 is the max size in a pnm file
        
        if (feof(fp)) {
            strcpy(str, "EOF");
            return str;
        }
    } while ((strlen(str) > 0) && (str[0] == '#'));
    
    return str;
}

void _readFile(FILE * fp, char ** content)
{
    long int origin_pos = ftell(fp);
    long int length = 0;
    fseek(fp, 0, SEEK_END);
    length = ftell(fp) - origin_pos;
    fseek(fp, origin_pos, SEEK_SET);
    
    *content = malloc(length * sizeof(char));
    
    if (*content == NULL) {
        printf("Error loading image. (1)\n");
        return;
    }
    
    fread(*content, sizeof(char), length, fp);
}

int _readInt(char ** content, unsigned int * out)
{    
    if (**content == '\0')
        return EOF;
    
    while (**content && strchr(" \n\t\r", **content))
        (*content)++;
    
    if (**content == '#') { // Skip comment
        while ((**content != '\n') && (**content != '\r')
               && (**content != '\0'))
            (*content)++;
        (*content)++;
        
        return _readInt(content, out);
    }
    
    *out = 0;
    while (!strchr(" \n\0\r\t", **content)) {
        *out *= 10;
        
        if ((**content >= '0') && (**content <= '9'))
            *out += (int)**content - (int)'0';
        else {
            puts("Cannot load image, bad input.");
            return EOF;
        }
        
        (*content)++;
    }
    
    return 0;
}

int _readChar(char ** content, char * out)
{    
    if (**content == '\0')
        return EOF;
    
    while (**content && strchr(" \n\t\r", **content))
        (*content)++;
    
    if (**content == '#') { // Skip comment
        while ((**content != '\n') && (**content != '\r')
               && (**content != '\0'))
            (*content)++;
        (*content)++;
        
        return _readChar(content, out);
    }
    
    *out = **content;
    (*content)++;
    
    return 0;
}

int _readWidthHeight(char ** content, unsigned int * w, unsigned int * h)
{
    if (_readInt(content, w))
        return EOF;
    
    return _readInt(content, h);
}

Image * _loadImagePBM(FILE * fp)
{
    Image * im = NULL;
    char * content;
    unsigned int width, height;
    unsigned int index; /* Index of the current pixel read */
    char current_px; /* Color of the current pixel (1/0) */
    
    _readFile(fp, &content);
    _readWidthHeight(&content, &width, &height);
    
    im = createImage(PBM, width, height);
    im->max_value = 1;
    
    for (index = 0; index < width * height; ++index) {
        if (_readChar(&content, &current_px) ||
            ((current_px != '0') &&
             (current_px != '1'))) {
            removeImage(im);
            return NULL;
        }
        im->pixels[index / width][index % width] =
            createPixelGreyscale(49 - (int)current_px);
    }
    
    return im;
}

Image * _loadImagePGM(FILE * fp)
{
    Image * im = NULL;
    char * content;
    unsigned int width, height;
    unsigned int index; /* Index of the current pixel read */
    component current_px; /* Color of the current pixel */
    
    _readFile(fp, &content);
    _readWidthHeight(&content, &width, &height);
    
    im = createImage(PGM, width, height);
    
    if (_readInt(&content, &im->max_value)) {
        removeImage(im);
        return NULL;
    }
    
    for (index = 0; index < width * height; ++index) {
        if (_readInt(&content, &current_px)) {
            removeImage(im);
            return NULL;
        }
        im->pixels[index / width][index % width] =
            createPixelGreyscale(CLAMP(current_px, 0, im->max_value));
    }
    
    return im;
}


Image * _loadImagePPM(FILE * fp)
{
    Image * im = NULL;
    char * content;
    unsigned int width, height;
    unsigned int index; /* Index of the current pixel read */
    component r, g, b; /* Current pixel colors */
    
    _readFile(fp, &content);
    _readWidthHeight(&content, &width, &height);
    
    im = createImage(PPM, width, height);
    
    if (_readInt(&content, &im->max_value)) {
        removeImage(im);
        return NULL;
    }
    puts("a");
    for (index = 0; index < width * height; ++index) {
        if (_readInt(&content, &r) ||
            _readInt(&content, &g) ||
            _readInt(&content, &b)) {
            removeImage(im);
            return NULL;
        }
        im->pixels[index / width][index % width] =
            createPixel(r, g, b);
    }
    
    return im;
}


Image * loadImage(const char * path)
{
    Image * im = NULL;
    FILE * fp = fopen(path, "r");
    char * format; // The format of the image (first line)
    
    if (!fp) {
        printf("Error loading image.\n");
        return NULL;
    }
    
    format = _readNextLine(fp);
    
    if (!strncmp(format, "P1", 2)) // P1 -> PBM
        im = _loadImagePBM(fp);
    else if (!strncmp(format, "P2", 2)) // P2 -> PGM
        im = _loadImagePGM(fp);
    else if (!strncmp(format, "P3", 2)) // P3 -> PPM
        im = _loadImagePPM(fp);
    else
        printf("Error loading image, bad image format. %s\n", format);
    
    fclose(fp);
    free(format);
    
    return im;
}
