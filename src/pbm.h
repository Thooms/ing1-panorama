#ifndef HEADER_PBM
#define HEADER_PBM

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
#define CLAMP(n,a,b) (MAX(MIN(n,b),a))

typedef uint32_t component;

typedef enum { 
    PBM, /** Black and white */ 
    PGM, /** Grey scale */
    PPM  /** Full RGB color */
} Format;

typedef struct {
    component r;
    component g;
    component b;
} Pixel;

/** Can represent any type of Netpbm image. */

typedef struct Image {
    
    /** Image original format. */
    Format origin_format; 
    
    unsigned int width;
    unsigned int height;

    /** In case of a PGM or a PPM format, this represents the maximum
	value for each component. It's set to a negative integer if
	the format is PBM. */
    unsigned int max_value;


    /** Each pixel is stored in a cell of the matrix.  For PBM and PGM
	formats, this is the only component.  The three components are
	stored at the same time, and we shall get it back via bitwise
	operations (see the concerned functions' documentation). */
    Pixel ** pixels;
    
} Image;

Pixel createPixel(component red, component green, component blue);

Pixel createPixelGreyscale(component c);

Image * createImage(Format format, int width, int height);

void removeImage(Image * im);

Image * copyImage(Image * im);

bool isPixelValid(int l, int c, Image * im);

#endif
