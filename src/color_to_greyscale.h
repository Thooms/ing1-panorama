#ifndef HEADER_COLOR_TO_GREYSCALE
#define HEADER_COLOR_TO_GREYSCALE

#include "pbm.h"

void colorToGrayscaleLightness(Image * im);
void colorToGrayscaleAverage(Image * im);
void colorToGrayscaleLuminosity(Image * im);

#endif
