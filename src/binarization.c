#include "binarization.h"


void binarization(Image * im, component treshold)
{
    if (treshold > im->max_value) 
	treshold = im->max_value;
    else if (treshold < 0) 
	treshold = 0;
    
    int i, j;

    component c;
    Pixel tmp;

    for (i = 0; i < im->height; i++)
	for (j = 0; j < im->width; j++)
	{
	    tmp = im->pixels[i][j];
	    c = (21 * tmp.r + 71 * tmp.g + 8 * tmp.b) / 100;

	    im->pixels[i][j] = 
		createPixelGreyscale(c >= treshold ? im->max_value : 0);  
	}
    
}
