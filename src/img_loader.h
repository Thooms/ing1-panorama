#ifndef HEADER_IMG_LOADER
#define HEADER_IMG_LOADER

#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include "pbm.h"

Image * loadImage(const char * path);

#endif
