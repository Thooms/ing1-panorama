#include <stdio.h>
#include <stdlib.h>

#include "pbm.h"
#include "color_to_greyscale.h"
#include "dilatation.h"
#include "erosion.h"
#include "img_loader.h"
#include "img_writer.h"

int main(void)
{
    Image * im = loadImage("../img/feep.pgm");
    //colorToGrayscaleAverage(im);
    //im->origin_format = PGM;
    erode(im);
    puts("Ready to write.");
    writeImage("save.pbm", im);
    removeImage(im);
    
    return EXIT_SUCCESS;
}
