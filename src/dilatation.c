#include "dilatation.h"

component _maxComponent(component pxs[5])
{
  int i;
  component max = pxs[0];
  
  for (i = 1; i < 5; ++i)
    if (pxs[i] > max)
      max = pxs[i];
  
  return max;
}

void dilate(Image * im)
{  
  if (im->origin_format == PPM) {
    puts("Cannot dilate PPM image.");
    return;
  }
  
  Image * copy = copyImage(im);
  int i, j;
  component current_pxs[5];
  component current_px;
  
  for (i = 0; i < im->height; ++i)
    for (j = 0; j < im->width; ++j) {
      current_pxs[0] = isPixelValid(i, j, copy) ? copy->pixels[i][j].r : 0;
      current_pxs[1] = isPixelValid(i-1, j, copy) ? copy->pixels[i-1][j].r : 0;
      current_pxs[2] = isPixelValid(i+1, j, copy) ? copy->pixels[i+1][j].r : 0;
      current_pxs[3] = isPixelValid(i, j-1, copy) ? copy->pixels[i][j-1].r : 0;
      current_pxs[4] = isPixelValid(i, j+1, copy) ? copy->pixels[i][j+1].r : 0;
      
      current_px = _maxComponent(current_pxs);
      im->pixels[i][j].r = current_px;
      im->pixels[i][j].g = current_px;
      im->pixels[i][j].b = current_px;
    }
  
  removeImage(copy);
}
