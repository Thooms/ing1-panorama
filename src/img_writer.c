#include "img_writer.h"

void _writeImagePBM(FILE * fp, Image * im)
{
    int i, j;
    
    fprintf(fp, "P1\n");
    fprintf(fp, "%i %i\n", im->width, im->height);
    
    for (i = 0; i < im->height; ++i)
        for (j = 0; j < im->width; ++j)
            fprintf(fp, "%i\n",
                    (im->max_value - im->pixels[i][j].r) / im->max_value);
}

void _writeImagePGM(FILE * fp, Image * im)
{
    int i, j;
    
    fprintf(fp, "P2\n");
    fprintf(fp, "%i %i\n", im->width, im->height);
    fprintf(fp, "%i\n", im->max_value);
    
    for (i = 0; i < im->height; ++i)
        for (j = 0; j < im->width; ++j)
            fprintf(fp, "%i\n", im->pixels[i][j].r);
}

void _writeImagePPM(FILE * fp, Image * im)
{
    int i, j;
    
    fprintf(fp, "P3\n");
    fprintf(fp, "%i %i\n", im->width, im->height);
    fprintf(fp, "%i\n", im->max_value);
    
    for (i = 0; i < im->height; ++i)
        for (j = 0; j < im->width; ++j)
            fprintf(fp, "%i %i %i\n",
                    im->pixels[i][j].r, im->pixels[i][j].g, im->pixels[i][j].b);
}

void writeImage(const char * path, Image * im)
{
    FILE * fp = fopen(path, "w");
    
    if (!fp) {
        printf("Cannot write image\n");
        return;
    }
    
    switch (im->origin_format) {
    case PBM:
        _writeImagePBM(fp, im);
        break;
    case PGM:
        _writeImagePGM(fp, im);
        break;
    case PPM:
        _writeImagePPM(fp, im);
        break;
    }
    
    fclose(fp);
}
